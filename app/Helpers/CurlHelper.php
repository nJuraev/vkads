<?php
/**
 * Created by PhpStorm.
 * User: Azamat
 * Date: 28.02.2019
 * Time: 8:20
 */

namespace App\Helpers;


class CurlHelper
{
    private static $vkAPIUrl="https://api.vk.com/method/";
    private static $version="5.92";
    static function  curl_get($action="",$token,$params=""){

        $curl=curl_init();



        curl_setopt($curl, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
         //curl_setopt($curl, CURLOPT_INTERFACE, '88.xx.xx.xx');
        curl_setopt($curl, CURLOPT_RETURNTRANSFER,true);
        curl_setopt($curl, CURLOPT_URL, self::$vkAPIUrl.$action."?access_token=".$token."&v=".self::$version.$params);
        $content = curl_exec( $curl );
        $err     = curl_errno( $curl );
        $errmsg  = curl_error( $curl );
        $header  = curl_getinfo( $curl );
        $header['errno']   = $err;
        $header['errmsg']  = $errmsg;
        $header['content'] = $content;

        return $header;
    }
}