<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Laravel\Socialite\SocialiteServiceProvider;
use Laravel\Socialite\SocialiteManager;
class SocialitePlusServiceProvider extends SocialiteServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind('Laravel\Socialite\Contracts\Factory', function ($app) {
            $socialiteManager = new SocialiteManager($app);

            $socialiteManager->extend('vk', function() use ($socialiteManager) {
                $config = $this->app['config']['services.vk'];

                return $socialiteManager->buildProvider(
                    'App\Auth\VkProvider', $config
                );
            });
            return $socialiteManager;
        });
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
