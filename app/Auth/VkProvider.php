<?php
/**
 * Created by PhpStorm.
 * User: Azamat
 * Date: 27.02.2019
 * Time: 20:57
 */

namespace App\Auth;


use Laravel\Socialite\Two\AbstractProvider;
use Laravel\Socialite\Two\ProviderInterface;
use Laravel\Socialite\Two\InvalidStateException;
use Laravel\Socialite\Two\User;
class VkProvider extends AbstractProvider implements ProviderInterface
{
    protected $apiUrl = 'https://api.vk.com';

    /**
     * VK API actual version
     *
     * @var string
     */
    protected $version = '5.92';

    /**
     * VK API Response language
     * On 'en' all data will be transliterated
     *
     * @var string
     */
    protected $lang = 'ru';

    /**
     * The scopes being requested
     *
     * @var array
     */
    protected $scopes = ['email','ads'];

    /**
     * User additional info fields
     *
     * By default returns: id, first_name, last_name
     *
     * @var array
     */
    protected $infoFields = [ 'photo_100', 'nickname', 'screen_name'];

    protected $stateless=true;
    /**
     * Get the authentication URL for the provider.
     *
     * @param  string $state
     * @return string
     */
    protected function getAuthUrl($state)
    {
        return $this->buildAuthUrlFromBase('https://oauth.vk.com/authorize', $state);
    }

    /**
     * Get the token URL for the provider.
     *
     * @return string
     */
    protected function getTokenUrl()
    {
        return 'https://oauth.vk.com/access_token';
    }

    /**
     * Get the raw user for the given access token.
     *
     * @param  string $token
     * @return array
     */
    protected function getUserByToken($token)
    {
        $access_token = $token['access_token'];

        $userInfoUrl = $this->apiUrl . '/method/users.get?access_token=' . $access_token
            . '&fields=' . implode(',', $this->infoFields) . '&lang=' . $this->lang . '&v=' . $this->version;

        $response = $this->getHttpClient()->get($userInfoUrl, [
            'headers' => [
                'Accept' => 'application/json',
            ],
        ]);

        $userData = json_decode($response->getBody(), true);
        $rawUser = reset($userData['response']);
        $rawUser['email'] = $token['email'];

        return $rawUser;
    }

    /**
     * Map the raw user array to a Socialite User instance.
     *
     * @param  array $user
     * @return \Laravel\Socialite\User
     */
    protected function mapUserToObject(array $user)
    {
        return (new User)->setRaw($user)->map([
            'id'       => $user['id'],
            'nickname' => $user['nickname'],
            'name'     => $user['first_name'] . ' ' . $user['last_name'],
            'email'    => isset($user['email']) ? $user['email'] : null,
            'avatar'   => $user['photo_100']
        ]);
    }

    /**
     * Override default by adding API version
     *
     * @param string $state
     * @return array
     */
    protected function getCodeFields($state = null)
    {
        $codeFields = parent::getCodeFields($state);
        $codeFields['v'] = $this->version;

        return $codeFields;
    }

    /**
     * Override in order to attach 'email' from requested permissions
     *
     * {@inheritdoc}
     */
    public function user()
    {
        if ($this->hasInvalidState()) {
            throw new InvalidStateException;
        }

        $token = $this->getAccessTokenResponse($this->getCode());
        $user = $this->mapUserToObject($this->getUserByToken($token));
        return $user->setToken($token['access_token']);
    }

    /**
     * Return all decoded data in order to retrieve additional params like 'email'
     *
     * {@inheritdoc}
     */
    protected function parseAccessToken($body)
    {
        return json_decode($body, true);
    }
    static function curl_get($action="",$token,$params=""){

        $curl=curl_init();

        curl_setopt($curl, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
        //curl_setopt($curl, CURLOPT_INTERFACE, '88.xx.xx.xx');
        curl_setopt($curl, CURLOPT_RETURNTRANSFER,true);
        curl_setopt($curl, CURLOPT_URL, "".$action."?access_token=".$token.$params);
        $content = curl_exec( $curl );
        $err     = curl_errno( $curl );
        $errmsg  = curl_error( $curl );
        $header  = curl_getinfo( $curl );
        $header['errno']   = $err;
        $header['errmsg']  = $errmsg;
        $header['content'] = $content;

        return $header;
    }
}