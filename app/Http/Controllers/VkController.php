<?php
/**
 * Created by PhpStorm.
 * User: Azamat
 * Date: 27.02.2019
 * Time: 21:10
 */

namespace App\Http\Controllers;
use App\Helpers\CurlHelper;
use Illuminate\Http\Request;
use Laravel\Socialite\Facades\Socialite;
use App\User;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;

class VkController extends Controller
{
    public function redirect()
    {
        return Socialite::driver("vk")->redirect();
    }

    public function callback()
    {
        try {
            $user = Socialite::driver('vk')->user();
           // dd($user);
            $input['name'] = $user->getName();
            $input['email'] = $user->getEmail();

            $input['vk_id'] = $user->getId();
            $input['token'] = $user->token;
            $input['avatar']= $user->avatar;
            $authUser = $this->findOrCreate($input);
            Auth::loginUsingId($authUser->id);

            return redirect()->route('myads');


        } catch (Exception $e) {
            //dd($e);
            return redirect('/');

        }
    }
    public function findOrCreate($input){
        $checkIfExist = User::where('vk_id',$input['vk_id'])
            ->first();

        if($checkIfExist){
            return $checkIfExist;
        }

        $user=new User();
        $user->name=$input['name'];
        $user->email=$input['email'];
        $user->password= Hash::make('123');
        $user->avatar= $input['avatar'];
        $user->vk_id=$input['vk_id'];
        $user->remember_token=$input['token'];
        $user->save();
        return $user;
    }

    public function myads(){

        $resp=CurlHelper::curl_get('ads.getAccounts',Auth::user()->getRememberToken());
        $content=json_decode($resp['content']);
        return view('vk/myads',['cabinets'=>$content->response]);
    }

    public function show_cabinet($account_id)
    {
        $resp=CurlHelper::curl_get('ads.getCampaigns',Auth::user()->getRememberToken(),'&account_id='.$account_id);
        //dd(json_decode($resp['content']));
        $content=json_decode($resp['content']);
        return view('vk/ads_cabinet',['account_id'=>$account_id,'companies'=>$content->response]);
    }

    public function show_company($account_id,$id)
    {
        $respcompany=CurlHelper::curl_get('ads.getCampaigns',Auth::user()->getRememberToken(),
            '&account_id='.$account_id."&campaign_ids=[".$id."]");
        $ccontent=json_decode($respcompany['content']);
        //dd($ccontent);
        $respads=CurlHelper::curl_get('ads.getAds',Auth::user()->getRememberToken(),
            '&account_id='.$account_id."&include_deleted=0&campaign_ids=[".$id."]");
        //dd(json_decode($respads['content']));
        $content=json_decode($respads['content']);
        $approved=$this->get_approved();
        $respcategory=CurlHelper::curl_get('ads.getCategories',Auth::user()->getRememberToken(),
            '&lang=ru');
        $cat_content=json_decode($respcategory['content']);
        //dd($cat_content);
        $categories=[];
        $parent="";
        $this->one_level_categories($cat_content->response->v1,$categories,$parent);
        $this->one_level_categories($cat_content->response->v2,$categories,$parent);
       // dd($categories);
        return view('vk/company_ads',[
            'account_id'=>$account_id,
            'company'=>$ccontent->response[0],
            'ads'=>$content->response,
            'approved'=>$approved,
            'categories'=>$categories,
            'status'=>$this->get_status(),
            'ad_format'=>$this->get_add_format()
        ]);
    }

    public function delete_ads($id,Request $request)
    {
        $account_id=$request->get('account_id');

        if (empty($account_id)){
            $request->session()->flash('error_msg','Необходимый параметр не передан');

            return redirect()->back();
        }
        $resp=CurlHelper::curl_get('ads.deleteAds',Auth::user()->getRememberToken(),
            '&account_id='.$account_id."&ids=[".$id."]");
        $content=json_decode($resp['content']);
        //dd($content);
        return redirect()->back()->with('success_msg','Успешно удалено');

    }


    function logout(){
        session()->flush();
        Auth::logout();
        return redirect()->route('home');
    }

    private function get_add_format()
    {
       return [
           1=>  "изображение и текст",
           2=>  "большое изображение",
           3=>  "эксклюзивный формат",
           4=>  "продвижение сообществ или приложений, квадратное изображение",
           5=>  "приложение в новостной ленте (устаревший)",
           6=>  "мобильное приложение",
           9=>  "запись в сообществе",
           11=>  "адаптивный формат",
       ] ;
    }

    private function get_approved ()
    {
        return [
            0 => "объявление не проходило модерацию",
            1 => "объявление ожидает модерации",
            2 => "объявление одобрено",
            3 => "объявление отклонено",
        ];
    }

    private function get_status()
    {
        return [
            0 => "объявление остановлено",
            1 => "объявление запущено",
            2 => "объявление удалено",
        ];
    }

    private function one_level_categories($categories,&$resarr,&$parent="")
    {
       foreach ($categories as $cat){
           if(isset($cat->subcategories)){
               $parent.=$cat->name ;
               $resarr[$cat->id]=$parent;
               $parent.=" > ";
               $this->one_level_categories($cat->subcategories,$resarr,$parent);
           }else{
               if($parent=="")
                   $resarr[$cat->id]=$cat->name;
               else{
                   $resarr[$cat->id]=$parent.$cat->name;

               }
           }

       }
        $parent="";
       return true;
    }

}