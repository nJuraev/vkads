<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
})->name('home');
Route::get('/login',function (){
    return view('welcome');
})->name('login');
Route::get('/vk/login',['uses'=>'VkController@redirect','as'=>'vk-login']);
Route::get('/vk/connect',['uses'=>'VkController@callback','as'=>'vk-connect']);
Route::get('/logout',['uses'=>'VkController@logout','as'=>'logout']);
Route::group(['Middleware'=>['web']],function() {
    Route::group(['middleware' => 'auth'], function () {
        Route::get('/vk/ads',['uses'=>'VkController@myads','as'=>'myads']);
        Route::get('/vk/ads/{account_id}',['uses'=>'VkController@show_cabinet','as'=>'show_ads_cabinet']);
        Route::get('/vk/ads/{account_id}/company/{id}',['uses'=>'VkController@show_company','as'=>'show_company']);

        Route::get('/vk/ads/{id}/delete',['uses'=>'VkController@delete_ads','as'=>'delete_ads']);
    });
});