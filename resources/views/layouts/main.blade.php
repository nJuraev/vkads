<!doctype html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link href="/css/bootstrap.min.css" rel="stylesheet">
    <link href="/css/style.css" rel="stylesheet">

    <title>@yield('title')</title>
    @yield('header')
</head>
<body>
 <!--Navigation -->

     <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
         <div class="container">
         <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarTogglerDemo01" aria-controls="navbarTogglerDemo01" aria-expanded="false" aria-label="Toggle navigation">
             <span class="navbar-toggler-icon"></span>
         </button>
         <div class="collapse navbar-collapse" id="navbarTogglerDemo01">
             <a class="navbar-brand" href="#">Vk ads</a>
             <ul class="navbar-nav mr-auto mt-2 mt-lg-0">
                 <li class="nav-item active">
                     <a class="nav-link" href="{{route('myads')}}">Главная <span class="sr-only">(current)</span></a>
                 </li>
                 <li class="nav-item">
                     <a class="nav-link" href="{{route('myads')}}">Рекламные кабинеты</a>
                 </li>
                 <li class="nav-item">
                     <a class="nav-link " href="#" tabindex="-1" aria-disabled="true">Компании</a>
                 </li>

             </ul>
             @if(Auth::user())
             <ul class="navbar-nav ml-auto mt-2 mt-lg-0">

                 <li>
                     <div class="inset">
                         <img src="{{Auth::user()->avatar}}">
                     </div>
                 </li>
                 <li><a class="nav-link ml-auto"  href="{{route('logout')}}">
                         {{Auth::user()->surname }} {{ Auth::user()->name}}(Выход)</a>
                 </li>

             </ul>
             @endif
         </div>
         </div>
     </nav>

<!-- Content -->
 <div class="container">
     <div class="row mt-5" >
         <div class="col-12">
             @yield('content')
         </div>
     </div>
 </div>
<!-- Content -->

 @yield('footer')
</body>
</html>