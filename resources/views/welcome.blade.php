@extends('layouts.main')
@section('title')
    Тестирование VK Ads
@endsection
  @section('header')
        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">

        <!-- Styles -->
        <style>
            html, body {
                background-color: #fff;
                color: #636b6f;
                font-family: 'Nunito', sans-serif;
                font-weight: 200;
                height: 100vh;
                margin: 0;
            }

            .flex-center {
                align-items: center;
                display: flex;
                justify-content: center;
            }

            .position-ref {
                position: relative;
            }



            .content {
                text-align: center;
            }

            .links > a {
                color: #636b6f;
                padding: 0 25px;
                font-size: 13px;
                font-weight: 600;
                letter-spacing: .1rem;
                text-decoration: none;
                text-transform: uppercase;
            }
        </style>
@endsection
@section('content')
        <div class="flex-center position-ref ">
            <div class="content">
                <div class="mt-5">
                    <div class="alert alert-info"> Для того чтоб посмотреть свои рекламные объявления <br> <a href="{{route('vk-login')}}" class="btn btn-success">
                            Войдите через VK
                        </a></div>

                </div>


            </div>
        </div>
@endsection
