@extends('layouts.main')
@section('title')
    Мои рекламные кабинеты
@endsection
@section('content')
    <h2> Список рекламных кабинетов</h2>
    <div class="row">
        <div class="col-10 ml-4">
            <ul class="list-group">
                @foreach($cabinets as $cab)
                    <a href="{{route('show_ads_cabinet',[$cab->account_id])}}">
                        <li class="list-group-item">
                           {{$cab->account_name}}
                        </li>
                    </a>
                @endforeach

            </ul>
        </div>
    </div>

@endsection
@section('footer')

@endsection