<?php
/**
Название кампании:	VkAds-tedt
Дневной лимит:	Не задан
Лимит объявления:	Не задан
Цена за 1000 показов:	30.4 рубля
Статус:	Остановлено (?)
Дата запуска:	Не задана
Дата остановки:	Не задана
Расписание:	Рабочее
Рекламные площадки:	Все площадки
Исключая стены сообществ
Исключая рекламную сеть
Ограничение показов:	Ограничивать до 5 показов на человека
Тематики:	Авто, мото
Сохранение аудитории:	Нет

Целевая аудитория:	7 400 человек
Страна:	Таджикистан
Интересы и поведение:	Автомобили
Ссылка:	http://vk.com/wall-178978841_2
 */?>
<table class="table table-sm">
    <tr>
        <td>Название объявления:</td>
        <td>{{$ad->name}}</td>
    </tr>
    <tr>
        <td>Название кампании:</td>
        <td>
            {{$company->name}}
        </td>
    </tr>

    <tr>
        <td>Категория:</td>
        <td>{{isset($categories[$ad->category1_id]) ? $categories[$ad->category1_id] : "Не указана" }}</td>
    </tr>
    <tr>
        <td>Формат объявления:</td>
        <td>{{$ad_format[$ad->ad_format]}}</td>
    </tr>
    <tr>
        <td>Статус:</td>
        <td>{{$status[$ad->status]}}</td>
    </tr>
    <tr>
        <td>Общий лимит:</td>
        <td>{{($ad->all_limit) ? $ad->all_limit : "Нет"}}</td>
    </tr>
    <tr>
        <td>Дневной лимит: </td>
        <td>{{($ad->day_limit) ? $ad->day_limit : "Нет"}}</td>
    </tr>

    <tr>
        <td>Тип оплаты:</td>
        <td>{{ ($ad->cost_type) ? "оплата за показы" : "оплата за переходы" }}</td>
    </tr>
    <tr>
        <td>Цена:</td>
        <td>{{ ($ad->cost_type) ? round($ad->cpm/100,2) : round($ad->cpc/100,2) }} руб</td>
    </tr>
    <tr>
        <td>Ограничение показов</td>
        <td>до {{$ad->impressions_limit}} показов</td>
    </tr>
    <tr>
        <td colspan="2">
            <a href="{{route('delete_ads',[$ad->id,'account_id'=>$account_id])}}" class="btn btn-outline-danger">Удалить объявление</a>
        </td>

    </tr>
</table>
