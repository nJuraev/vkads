@extends('layouts.main')
@section('title')
    Список рекламных объявлений
@endsection
@section('content')
<h2>Список рекламных объявлений</h2>
<div class="row">
    <div class="col-10 ml-4">
        <a href="{{route("show_ads_cabinet",[$account_id])}}" class="btn btn btn-outline-primary">Вернутся к списку компаний </a><br>
        <ul class="list-group mt-3">
            @foreach($ads as $ad)
                <li class="list-group-item">
                    @include('vk/ads_info',[
                    'ad'=>$ad,
                    'categories'=>$categories,
                    'company'=>$company,
                    'approved'=>$approved,
                    'status'=>$status,
                    'ad_format'=>$ad_format,
                    'account_id'=>$account_id
                    ])
                </li>
            @endforeach

        </ul>
    </div>
</div>
    @endsection