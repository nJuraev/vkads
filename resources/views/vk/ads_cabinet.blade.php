@extends('layouts.main')
@section('title')
    Список рекламных компаний
@endsection
@section('content')
    <h2> Список компаний</h2>
    <div class="row">
        <div class="col-10 ml-4">
            <a href="{{route("myads")}}" class="btn btn btn-outline-primary">Вернутся к списку рекламных кабинетов </a><br>
            <ul class="list-group mt-3">
                @foreach($companies as $company)
                    <a href="{{route('show_company',[$account_id,$company->id])}}">
                        <li class="list-group-item">
                            {{$company->name}}
                        </li>
                    </a>
                @endforeach
            </ul>
        </div>
    </div>

@endsection
@section('footer')

@endsection